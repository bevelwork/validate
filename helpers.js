module.exports = {
  testValidityOfInput: function (value) {
    // Handle strings
    if (
      typeof value == "string" &&
      value.length <= 255 // Confirm Type of "string"
    )
      return true;

    // Handle numbers
    if (typeof value == "number" && String(value) <= 255) return true;
    return false;
  },

  //Check if items in array have hyphen
  checkForHyphen: function (value) {
    if (typeof value == "string") return value.includes("-");
    return false;
  },

  //Count number of hyphens in array
  countTheHyphens: function (value) {
    if (typeof value == "string") return value.split("-").length - 1;
    return false;
  },

  //Count number of hyphens in array
  verifyDate: function (value) {
    const regex_date = /^\d{4}\-\d{2}\-\d{2}$/;
    //Check for the pattern
    if (regex_date.test(value)) {
      return true;
    } else return false;
  },

  //Count number of hyphens in array
  verifyTime: function (value) {
    const regex_time = /^\d{2}:(?:[0-5]\d):(?:[0-5]\d)$/;
    //Check for the pattern
    if (!regex_time.test(value)) {
      return "Incorrect format.";
    } else return "Valid format";
  },
};
